# Tech-Picks

A bunch of cool technologies people should know about

## Concepts
* FaaS - Function as a Service
* IaC - Infrastructure as Code

## Devops
* Gitlab CI/CD
* Locust
* Spinnaker
* Ansible
* Terraform
* Spinnaker

## Chaos Engineering Tools
* Chaos Monkey
* Chaos Mesh
* Chaostoolkit

## Load testing
* Locust

## Data streams & collection
* The elastic stack
    * Elasticsearch
    * Kibana
    * Logstash
    * Filebeat
    * Metricbeat
    * APM
* Kafka
* Fluentd
* Prometheus
* Grafana
* Airflow
* Presto

## Task execution & orcestration
* Celery
* Argo Workflow

## Development & deployment tools
* Helm
* Skaffold
* docker-compose

## Authentication & authorization
* Keycloak (or the enterprise edition: Redhat SSO)

## Application Gateway
* nginx
* Traefik

## Kubernetes concepts
* CRD - Custom Resource Definition
* Sidecar container

## Kubernetes useful resources
* Cert manager + Lets encrypt free certificate + Azure DNS - check out [this tutorial](https://gist.github.com/marcopaga/1b6d045d85099cbf32456443a6e3cdf7)

